'use strict';

angular.module('myApp', [
  'myApp.controllers'
]);

angular.module('myApp.controllers', []).
    controller('Controlador', function($scope) {
        $scope.nombre = "Hugo";
        $scope.numero1 = 0;
        $scope.numero2 = 0;
    }).
    controller('Calculadora', function($scope) {
        $scope.titulo = "Angular Calculadora";
        $scope.digitos = 0;
        $scope.numero1 = 0;
        $scope.numero2 = 0;
        $scope.operacion = null;
        $scope.seleccionar = function(valor) {
            if($scope.digitos == 0) {
                $scope.digitos = valor;
                return;
            }
            $scope.digitos = $scope.digitos + "" + valor;
        };
        $scope.escogerOperacion = function(operacion) {
            $scope.operacion = operacion;
            $scope.numero1 = parseInt($scope.digitos);
            $scope.digitos = 0;
        };
        $scope.igual = function() {
            console.info($scope.operacion);
            console.info($scope.numero1);
            $scope.numero2 = parseInt($scope.digitos);
            console.info($scope.numero2);
            if($scope.operacion == "suma") {
                $scope.digitos = $scope.numero1 + $scope.numero2;
            }
        };
    });